//
//  DigiBitPluginAppController.mm
//  iosplugin
//
//  Created by Muhammad Uzair on 9/8/17.
//  Copyright © 2017 DigiBit. All rights reserved.
//

#import "iosplugin.h"
#import "UnityAppController.h"


@interface DigiBitPluginAppController : UnityAppController

@property ( retain, nonatomic ) iosplugin* NativeiOSPlugin;

-(void) initNativePlugin;

@end

static DigiBitPluginAppController* DelegateObject;

@implementation DigiBitPluginAppController

@synthesize NativeiOSPlugin;



//PLUGIN INITIALIZATION FROM UNITY
-(void) startUnity: (UIApplication*) application {
    [super startUnity: application];  //call the super.
    [self initNativePlugin];
    DelegateObject = self;
}


-(void) initNativePlugin {
    NativeiOSPlugin = [[ iosplugin alloc ] init];
}
//PLUGIN INITIALIZATION FROM UNITY





//PLUGIN CALLS FROM DIGIBITPLUGINCONRTOLLER
-(void) setSerial_:(NSString*) Serial {
    [NativeiOSPlugin setSerial:Serial ];
}

-(void) setObjectName_:(NSString*) ObjectName {
    [NativeiOSPlugin setObjectName:ObjectName ];
}

-(bool) sendCommand_:(NSString*) Command {
   return [NativeiOSPlugin sendCommand:Command ];
}

-(NSString*) getDigibitPidData_ {
    return [NativeiOSPlugin getDigibitPidData];
}

-(NSString*) getTimeStampDiffPrimary_ {
    return [NativeiOSPlugin getTimeStampDiffPrimary];
}

-(NSString*) getTimeStampDiffSecondary_ {
    return [NativeiOSPlugin getTimeStampDiffSecondary];
}

-(NSString*) getTimeStampDiff_ {
    return [NativeiOSPlugin getTimeStampDiff];
}

-(void) enableLogs_:(bool) Value {
    [NativeiOSPlugin enableLogs:Value ];
}

-(void) onApplicationPaused_:(bool) Paused {
    [NativeiOSPlugin onApplicationPaused:Paused ];
}

//PLUGIN CALLS FROM DIGIBITPLUGINCONRTOLLER




//PLUGIN CALLS FROM UNITY
extern "C" {
    
    
    void SetSerial( const char* _Serial ){
        NSString* Serial = [[NSString alloc] initWithUTF8String:_Serial];
        [DelegateObject setSerial_:Serial];
    }
    
    void SetObjectName( const char* _ObjectName ){
        NSString* ObjectName = [[NSString alloc] initWithUTF8String:_ObjectName];
        [DelegateObject setObjectName_:ObjectName];
    }
    
    bool SendCommand( const char* _Command ){
        NSString* Command = [[NSString alloc] initWithUTF8String:_Command];
        return [DelegateObject sendCommand_:Command];
    }
    
    char* getDigibitPidData(){
        NSString* Data = [DelegateObject getDigibitPidData_];
        return (char*) [Data UTF8String];
    }
    
    char* getTimeStampDiffPrimary(){
        NSString* Data = [DelegateObject getTimeStampDiffPrimary_];
        return (char*) [Data UTF8String];
    }
    
    char* getTimeStampDiffSecondary(){
        NSString* Data = [DelegateObject getTimeStampDiffSecondary_];
        return (char*) [Data UTF8String];
    }
    
    char* getTimeStampDiff(){
        NSString* Data = [DelegateObject getTimeStampDiff_];
        return (char*) [Data UTF8String];
    }
    
    void EnableLogs( bool Value ){
        [DelegateObject enableLogs_:Value];
    }
    
    void OnApplicationPaused( bool Paused ){
        [DelegateObject onApplicationPaused_:Paused];
    }
}
//PLUGIN CALLS FROM UNITY

@end

//setting this as app controller.
IMPL_APP_CONTROLLER_SUBCLASS( DigiBitPluginAppController)
