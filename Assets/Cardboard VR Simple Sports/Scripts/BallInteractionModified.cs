﻿using System.Collections;
using UnityEngine;
using DigiBitSDK;

public class BallInteractionModified : MonoBehaviour
{

     // this variable is used to know which ball is selected
     public GameObject selectedBALL;
     // this is the head gameObject
     public Transform head;
     // this is the force value 
     public float throwingForce = 20f;
     public Vector3 StartPosition;
     // this is the rigid body attached to the ball
     Rigidbody rigBody;

     float pitch = 0.0f;
     bool ballInHand = true;
     Vector3 deltaHead = Vector3.zero;
     Vector3 previousHead;


     //DigiBit ball motion
     float pitchMax = -60.0f;
     float pitchMin = 60.0f;

     float posMin = 0.21f;
     float posMax = 2.2f;


     void Start()
     {

          rigBody = selectedBALL.GetComponent<Rigidbody>();
          StartPosition = selectedBALL.transform.position;
          previousHead = head.forward;
     }



     void Update()
     {
          if ((Input.GetKeyDown(KeyCode.Space) || (DigiBit.PrimaryData.MoveForward && DigiBit.SecondaryData.MoveForward)) && ballInHand)
               Shoot();
          if (Input.GetKeyDown(KeyCode.UpArrow) && ballInHand)
          {
               pitch += 0.1f;
               MoveUpDown(pitch);
          }
          if (Input.GetKeyDown(KeyCode.DownArrow) && ballInHand)
          {
               pitch -= 0.1f;
               MoveUpDown(pitch);
          }

          MoveLeftRight();

          if (ballInHand && DigiBit.PrimaryConnected && DigiBit.SecondaryConnected)
               MoveUpDownDigiBit();
     }

     void MoveLeftRight()
     {

          deltaHead.x = head.forward.x - previousHead.x;
          deltaHead.z = head.forward.z - previousHead.z;

          previousHead = head.forward;

          selectedBALL.transform.position += new Vector3(deltaHead.x, 0, deltaHead.z) * 1.2f;
     }

     void MoveUpDown(float Pitch)
     {

          Vector3 NewPosition = new Vector3(selectedBALL.transform.position.x, Pitch, selectedBALL.transform.position.z);
          selectedBALL.transform.position = Vector3.Lerp(selectedBALL.transform.position, NewPosition, 0.5f);
          rigBody.velocity = new Vector3(0, 0, 0);
          rigBody.useGravity = false;
     }

     void MoveUpDownDigiBit()
     {
          float Pitch = (DigiBit.PrimaryData.EulerAngle.y + DigiBit.SecondaryData.EulerAngle.y) / 2.0f;

          if (Pitch < pitchMax)
               Pitch = pitchMax;
          else if (Pitch > pitchMin)
               Pitch = pitchMin;

          float y = Map(Pitch, pitchMin, pitchMax, posMin, posMax);
          Vector3 NewPosition = new Vector3(selectedBALL.transform.position.x, y, selectedBALL.transform.position.z);
          selectedBALL.transform.position = Vector3.Lerp(selectedBALL.transform.position, NewPosition, 0.5f);
          rigBody.velocity = new Vector3(0, 0, 0);
          rigBody.useGravity = false;
     }

     void Shoot()
     {

          ballInHand = false;
          Debug.Log(head.right);
          rigBody.velocity = throwingForce * 0.8f * new Vector3(head.forward.x, 0, head.forward.z) + throwingForce * 0.9f * Vector3.up;
          rigBody.useGravity = true;
          StartCoroutine(ResetBall());

     }

     IEnumerator ResetBall()
     {

          yield return new WaitForSeconds(3.0f);

          selectedBALL.transform.position = StartPosition;
          rigBody.velocity = new Vector3(0, 0, 0);
          ballInHand = true;
     }

     float Map(float x, float in_min, float in_max, float out_min, float out_max)
     {
          return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
     }
}
