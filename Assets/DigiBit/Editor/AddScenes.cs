﻿using UnityEngine;
using UnityEditor;

[InitializeOnLoad]
class AddPluginScenes
{
	static AddPluginScenes ()
	{
		
		bool PluginSceneAdded = false;

		string PluginSceneLocation   = "Assets/DigiBit/Scenes/PluginStartUp.unity";
		EditorBuildSettingsScene[] OriginalList = EditorBuildSettings.scenes;

		foreach (EditorBuildSettingsScene scene in OriginalList) {
			if (scene.path == PluginSceneLocation)
				PluginSceneAdded = true;
		}	

		if (!PluginSceneAdded) {
			EditorBuildSettingsScene[] NewList = new EditorBuildSettingsScene[OriginalList.Length + 1];
			System.Array.Copy(OriginalList, NewList, OriginalList.Length);
			NewList[NewList.Length-1] = new EditorBuildSettingsScene(PluginSceneLocation, true);

			EditorBuildSettings.scenes = NewList;
		}

	//	EditorApplication.update += Update;
	}

}