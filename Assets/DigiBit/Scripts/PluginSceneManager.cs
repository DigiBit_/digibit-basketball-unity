﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using DigiBitSDK;
using UnityEngine.XR;

public class PluginSceneManager : MonoBehaviour {

	//edit this script as necessary but keep the IsActive bool there to check for multiple instance of scene
	//Led pattern requires new firmware
	//game should be paused when digibits disconnect as this script does not handle game pause

	static bool IsActive = false; //to prevent multiple instances of scene to load
	static bool FirstLoad = true;

	public GameObject FlashingPanel;
	public GameObject WaitingForConnectionPanel;
	public GameObject WaitingForGameToStartPanel;
	public GameObject FWInfoPanel;
	public GameObject LeftPanel;
	public GameObject RightPanel;

	public GameObject RightHand;
	public GameObject LeftHand;
	public GameObject RightFoot;
	public GameObject LeftFoot;

	public GameObject RightRedLED1;
	public GameObject RightRedLED2;
	public GameObject RightRedLED3;
	public GameObject LeftRedLED1;
	public GameObject LeftRedLED2;
	public GameObject LeftRedLED3;
	public GameObject RightGreenLED1;
	public GameObject RightGreenLED2;
	public GameObject RightGreenLED3;
	public GameObject LeftGreenLED1;
	public GameObject LeftGreenLED2;
	public GameObject LeftGreenLED3;
	public GameObject RightBlueLED1;
	public GameObject RightBlueLED2;
	public GameObject RightBlueLED3;
	public GameObject LeftBlueLED1;
	public GameObject LeftBlueLED2;
	public GameObject LeftBlueLED3;
	public GameObject RightWhiteLED1;
	public GameObject RightWhiteLED2;
	public GameObject RightWhiteLED3;
	public GameObject LeftWhiteLED1;
	public GameObject LeftWhiteLED2;
	public GameObject LeftWhiteLED3;

	public Text GameResumeCounter;
	public Text FWVersionInfoText;
	public Text ConfirmText;

	public float digitAnimationDistance = 1.8f;
	public float digitAnimationSpeed = 10.0f;
	public float smallNumber = 0.00001f;

	public bool PlayWithoutDigiBits = false;
	public bool WaitBeforeResuming = true;

	private bool LEDDefaultPattern = true;
	private Vector3  rightPosStart;
	private Vector3  leftPosStart;

	enum CalibrationStage {
		StageRight,
		CalibratingRight,
		StageLeft,
		CalibratingLeft,
		Done,
	};

	CalibrationStage PlayerStage = CalibrationStage.StageRight; 

	Scene CurrentScene;
	bool EndScene;
	int RequiredFirmwareVersion = 4;

	void Awake()
	{
		if (IsActive) {
			SceneManager.UnloadSceneAsync ("PluginStartUp");
			return;
		}

		IsActive = true;
	}

	void Start () {

		if (XRSettings.loadedDeviceName == "cardboard") {
			ContinueGame ();
			return;
		}
		
		FlashingPanel.SetActive (false);
		WaitingForConnectionPanel.SetActive (true);

		CurrentScene = SceneManager.GetActiveScene();
		PlayerStage = CalibrationStage.StageRight;

		ManageCameraPosition ();
		ManageEventSystem ();
		ManageSortingOrder ();

		StartCoroutine (FirmwareInfo ());
		DefaultLEDs ();

		if (DigiBit.OnFeet) {				
			RightHand.SetActive (false);
			LeftHand.SetActive (false);
			RightFoot.SetActive (true);
			LeftFoot.SetActive (true);
		} else {
			RightHand.SetActive (true);
			LeftHand.SetActive (true);
			RightFoot.SetActive (false);
			LeftFoot.SetActive (false);
		}
	}

	void Update ()
	{
		if (PlayWithoutDigiBits)
			GoToGame ();

		if (EndScene)
			return;

		if (DigiBit.PrimaryData.FirmwareVersion >= 0 && DigiBit.SecondaryData.FirmwareVersion >= 0) {
			WaitingForConnectionPanel.SetActive (false);
			FlashingPanel.SetActive (true);
		} else {
			WaitingForConnectionPanel.SetActive (true);
			FlashingPanel.SetActive (false);
		}
	}

	IEnumerator FirmwareInfo()
	{
		int counter = 0;
		while (true) {
			if (DigiBit.PrimaryData.FirmwareVersion == -1 || DigiBit.SecondaryData.FirmwareVersion == -1) {
				FWVersionInfoText.GetComponent<Text> ().text = "Firmware version not found." + counter++;
				yield return new WaitForSecondsRealtime (0.15f);
			} else if ((DigiBit.PrimaryData.FirmwareVersion < RequiredFirmwareVersion &&
			           DigiBit.SecondaryData.FirmwareVersion <= RequiredFirmwareVersion)) {	

				FWVersionInfoText.GetComponent<Text> ().text = "Old firmware: " + RequiredFirmwareVersion + " Version found: " + 
					DigiBit.PrimaryData.FirmwareVersion + " and " + DigiBit.SecondaryData.FirmwareVersion + "\n Use DigiBit Connect to update." + counter++;

				FWInfoPanel.SetActive (false);
				yield return new WaitForSeconds (0.1f);
				FWInfoPanel.SetActive (true);
				yield return new WaitForSeconds (0.1f);
				FWInfoPanel.SetActive (false);
				yield return new WaitForSeconds (0.1f);
				FWInfoPanel.SetActive (true);
				yield return new WaitForSeconds (1.5f);
				FWInfoPanel.SetActive (false);
			} else
				FWVersionInfoText.GetComponent<Text> ().text = " ";

			yield return new WaitForSecondsRealtime (0.15f);
		}

	}

	void ManageCameraPosition()
	{
		float MaxZposition = 0.0f;
		if (CurrentScene.name != "PluginStartUp") {
			foreach (GameObject GameObj in CurrentScene.GetRootGameObjects()) {

				foreach (Transform T in GameObj.GetComponentsInChildren<Transform> (true)) {
					if (MaxZposition < T.position.z)
						MaxZposition = T.position.z;
				}
			}
		}

		Transform CameraTransform = gameObject.GetComponentInChildren<Camera> ().gameObject.transform;
		CameraTransform.position = new Vector3 (CameraTransform.position.x, CameraTransform.position.y, MaxZposition+1.0f);
	}

	void ManageEventSystem()
	{
		if (GameObject.FindObjectOfType<EventSystem> () == null)
			gameObject.GetComponentInChildren<EventSystem>(true).gameObject.SetActive(true);
	}

	void ManageSortingOrder()
	{
		int SortOrder = 1000; //need better way to find this in scene
		string SortingLayerName = SortingLayer.layers [SortingLayer.layers.Length - 1].name;
		FlashingPanel.GetComponent<Canvas> ().sortingLayerName = SortingLayerName;
		FlashingPanel.GetComponent<Canvas> ().sortingOrder= SortOrder;
		WaitingForConnectionPanel.GetComponent<Canvas> ().sortingLayerName = SortingLayerName;
		WaitingForConnectionPanel.GetComponent<Canvas> ().sortingOrder= SortOrder;
	}		
	
	#region ColorButtonCallbacks
	public void SetLEDs(Devices deviceSelection, LED led1, LED led2, LED led3)
	{		
		if (deviceSelection == Devices.BOTH || deviceSelection == Devices.PRIMARY) {
			LeftRedLED1.SetActive (led1.Red!=0);
			LeftWhiteLED1.SetActive (led1.White!=0);
			LeftBlueLED1.SetActive (led1.Blue!=0);
			LeftGreenLED1.SetActive (led1.Green!=0);

			LeftRedLED2.SetActive (led2.Red!=0);
			LeftWhiteLED2.SetActive (led2.White!=0);
			LeftBlueLED2.SetActive (led2.Blue!=0);
			LeftGreenLED2.SetActive (led2.Green!=0);

			LeftRedLED3.SetActive (led3.Red!=0);
			LeftWhiteLED3.SetActive (led3.White!=0);
			LeftBlueLED3.SetActive (led3.Blue!=0);
			LeftGreenLED3.SetActive (led3.Green!=0);
		}

		if (deviceSelection == Devices.BOTH || deviceSelection == Devices.SECONDARY) {
			RightRedLED1.SetActive (led1.Red!=0);
			RightWhiteLED1.SetActive (led1.White!=0);
			RightBlueLED1.SetActive (led1.Blue!=0);
			RightGreenLED1.SetActive (led1.Green!=0);

			RightRedLED2.SetActive (led2.Red!=0);
			RightWhiteLED2.SetActive (led2.White!=0);
			RightBlueLED2.SetActive (led2.Blue!=0);
			RightGreenLED2.SetActive (led2.Green!=0);

			RightRedLED3.SetActive (led3.Red!=0);
			RightWhiteLED3.SetActive (led3.White!=0);
			RightBlueLED3.SetActive (led3.Blue!=0);
			RightGreenLED3.SetActive (led3.Green!=0);
		}

		DigiBit.SetLEDs (deviceSelection, led3, led2, led1);
	}

	public void AllLEDsOff()
	{
		LEDDefaultPattern = false;
		SetLEDs (Devices.BOTH, new LED (0, 0, 0, 0), new LED (0, 0, 0, 0), new LED (0, 0, 0, 0));
	}

	public void BlueOn()
	{
		AllLEDsOff ();
		SetLEDs (Devices.BOTH, new LED (0, 0, 255, 0), new LED (0, 0, 255, 0), new LED (0, 0, 255, 0));
	}

	public void DefaultLEDs()
	{
		AllLEDsOff ();

		LEDDefaultPattern = true;
		StartCoroutine (FlashDefaultLEDs ());
	}

	IEnumerator FlashDefaultLEDs()
	{
		LED ONGREEN = new LED(0, 255, 0, 0);
		LED ONBLUE = new LED(0, 0, 255, 0);
		LED OFF = new LED(0, 0, 0, 0);

		while (LEDDefaultPattern) {				

			for (int i = 0; i < 2; i++) {				
				SetLEDs (Devices.PRIMARY, ONGREEN, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, ONGREEN);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.BOTH, OFF, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
			}

			for (int i = 0; i < 2; i++) {				
				SetLEDs (Devices.SECONDARY, ONBLUE, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, ONBLUE);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
				SetLEDs (Devices.BOTH, OFF, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				if (!LEDDefaultPattern) break;
			}
		}
	}

	public void IdentifyWithRed()
	{
		DigiBit.DigiBitIdentify (LEDColor.RED);
		StartCoroutine (FlashRed ());
	}

	IEnumerator FlashRed()
	{
		AllLEDsOff ();
		LED ON = new LED(255, 0, 0, 0);
		LED OFF = new LED(0, 0, 0, 0);

		for (int i = 0; i < 6; i++) {
			SetLEDs (Devices.BOTH, ON, ON, ON);
			yield return new WaitForSecondsRealtime (0.3f);

			SetLEDs (Devices.BOTH, OFF, OFF, OFF);
			yield return new WaitForSecondsRealtime (0.3f);
		}
		DefaultLEDs ();
	}

	public void IdentifyWithGreen()
	{
		DigiBit.DigiBitIdentify (LEDColor.GREEN);
		StartCoroutine (FlashGreen ());
	}

	IEnumerator FlashGreen()
	{
		AllLEDsOff ();
		LED ON = new LED(0, 255, 0, 0);
		LED OFF = new LED(0, 0, 0, 0);

		for (int i = 0; i < 6; i++) {
			SetLEDs (Devices.BOTH, ON, ON, ON);
			yield return new WaitForSecondsRealtime (0.3f);

			SetLEDs (Devices.BOTH, OFF, OFF, OFF);
			yield return new WaitForSecondsRealtime (0.3f);
		}
		DefaultLEDs ();
	}

	public void IdentifyWithBlue()
	{
		StartCoroutine (FlashBlue ());
	}

	IEnumerator FlashBlue()
	{
		AllLEDsOff ();
		LED ON = new LED(0, 0, 255, 0);
		LED OFF = new LED(0, 0, 0, 0);

		for (int i = 0; i < 6; i++) {
			SetLEDs (Devices.BOTH, ON, ON, ON);
			yield return new WaitForSecondsRealtime (0.3f);

			SetLEDs (Devices.BOTH, OFF, OFF, OFF);
			yield return new WaitForSecondsRealtime (0.3f);
		}
		DefaultLEDs ();
	}

	public void IdentifyWithRWhite()
	{
		StartCoroutine (FlashWhite ());
	}

	IEnumerator FlashWhite()
	{
		AllLEDsOff ();
		LED ON = new LED(0, 0, 0, 255);
		LED OFF = new LED(0, 0, 0, 0);

		for (int i = 0; i < 6; i++) {
			SetLEDs (Devices.BOTH, ON, ON, ON);
			yield return new WaitForSecondsRealtime (0.3f);

			SetLEDs (Devices.BOTH, OFF, OFF, OFF);
			yield return new WaitForSecondsRealtime (0.3f);
		}
		DefaultLEDs ();
	}

	#endregion

	public void GoToGame()
	{
		SceneManager.UnloadSceneAsync("PluginStartUp");
		IsActive = false;
		FirstLoad = false;
	}

	IEnumerator ResumeGameWithDelay()
	{
		WaitingForGameToStartPanel.SetActive (true);
		WaitingForConnectionPanel.SetActive (false);
		FlashingPanel.SetActive (false);

		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 3";
		yield return new WaitForSecondsRealtime (1.0f);
		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 2";
		yield return new WaitForSecondsRealtime (1.0f);
		GameResumeCounter.GetComponent<Text> ().text = "Resuming Game in 1";
		yield return new WaitForSecondsRealtime (1.0f);

		SceneManager.UnloadSceneAsync("PluginStartUp");
		IsActive = false;
		FirstLoad = false;
	}

    IEnumerator CalibrateLeftRight()
	{
		rightPosStart = RightPanel.transform.position;
		leftPosStart = LeftPanel.transform.position;

		while (true) {
			yield return new WaitForSecondsRealtime (0.128f);			
			if (PlayerStage == CalibrationStage.StageRight) {
				if (DigiBit.OnFeet)
					ConfirmText.text = "Step RIGHT foot forward and back.\n(waiting...)";
				else
					ConfirmText.text = "Quickly move RIGHT forward and back.\n(waiting...)";
				PlayerStage = CalibrationStage.CalibratingRight;				
				StartCoroutine (RightCalibrate ());
				continue;
			} else if (PlayerStage == CalibrationStage.StageLeft) {
				if (DigiBit.OnFeet)
					ConfirmText.text = "Step LEFT foot forward and back.\n(waiting...)";
				else
					ConfirmText.text = "Quickly move LEFT forward and back.\n(waiting...)";
				PlayerStage = CalibrationStage.CalibratingLeft;
				StartCoroutine (LeftCalibrate ());
				continue;
			} else if (PlayerStage == CalibrationStage.Done) {
				if (DigiBit.OnFeet)
					ConfirmText.text = "Done! Feet on screen follow your feet.\n(press to continue)";
				else
					ConfirmText.text = "Done! Hands on screen follow your hands.\n(press to continue)";
				StartCoroutine (HandsFollowAnimation ());
				break;
			}
		}
	}

	public void ContinueGame()
	{
		if (PlayerStage == CalibrationStage.StageRight) {
			StartCoroutine (CalibrateLeftRight ());
		}
		else if (PlayerStage == CalibrationStage.Done) {
			EndScene = true;
//			if (WaitBeforeResuming && !FirstLoad)
//				StartCoroutine (ResumeGameWithDelay ());
//			else
				GoToGame ();
		}
	}

	IEnumerator RightCalibrate()
	{
		var posEnd = rightPosStart; 
		posEnd.y += digitAnimationDistance;

		LED ONGREEN = new LED(0, 255, 0, 0);
		LED ONBLUE = new LED(0, 0, 255, 0);
		LED OFF = new LED(0, 0, 0, 0);

		AllLEDsOff ();
		yield return new WaitForSecondsRealtime (0.25f);
		SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, ONGREEN);
		yield return new WaitForSecondsRealtime (0.25f);
		SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, ONBLUE);

        // This loop is to ensure we get the commnd sent and verification that is is recieved.
		Debug.Log ("CalState: CalibrateRight() Starting CMD");
		DigiBit.PrimaryData.CalState = DigiBit.SecondaryData.CalState = CalibrationState.NOT_CALIBRATED;
        while (DigiBit.PrimaryData.CalState == CalibrationState.NOT_CALIBRATED ||
               DigiBit.SecondaryData.CalState == CalibrationState.NOT_CALIBRATED){
               DigiBit.CalibrateRight();
               yield return new WaitForSecondsRealtime(0.3f);
        }
		Debug.Log ("CalState: CalibrateRight() CMD Rx'ed");

	    while (PlayerStage == CalibrationStage.CalibratingRight) {		
			if ((RightPanel.transform.position - posEnd).sqrMagnitude > smallNumber) {
				RightPanel.transform.position = Vector3.MoveTowards (RightPanel.transform.position, posEnd, digitAnimationSpeed * Time.deltaTime);
			} else if ((RightPanel.transform.position - rightPosStart).sqrMagnitude > smallNumber) {
				posEnd = rightPosStart;
			} else {
				posEnd = rightPosStart;
				posEnd.y += digitAnimationDistance;

				SetLEDs (Devices.SECONDARY, OFF, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.5f);
				SetLEDs (Devices.SECONDARY, ONBLUE, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.5f);
				SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, OFF);
				yield return new WaitForSecondsRealtime (0.5f);
				SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, ONBLUE);
				yield return new WaitForSecondsRealtime (0.5f);
			}
			
			yield return null;

			if (PlayerStage == CalibrationStage.CalibratingRight && 
					(DigiBit.SecondaryData.CalState == CalibrationState.SUCCESS_FINISHED && DigiBit.PrimaryData.CalState != CalibrationState.SUCCESS_FINISHED) ||
					(DigiBit.SecondaryData.CalState != CalibrationState.SUCCESS_FINISHED && DigiBit.PrimaryData.CalState == CalibrationState.SUCCESS_FINISHED)) {
				PlayerStage = CalibrationStage.StageLeft;
				Debug.Log ("CalState: CalibrationStage.StageLeft");
			}
		}
	}

	IEnumerator LeftCalibrate()
	{	
		var posEnd = leftPosStart; 
		posEnd.y += digitAnimationDistance;

		LED ONGREEN = new LED (0, 255, 0, 0);
		LED ONBLUE = new LED (0, 0, 255, 0);
		LED OFF = new LED (0, 0, 0, 0);

		AllLEDsOff ();
		yield return new WaitForSecondsRealtime (0.25f);
		SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, ONGREEN);
		yield return new WaitForSecondsRealtime (0.25f);
		SetLEDs (Devices.SECONDARY, ONBLUE, ONBLUE, ONBLUE);

		// This loop is to ensure we get the commnd sent and verification that is is recieved.
		DigiBit.PrimaryData.CalState = DigiBit.SecondaryData.CalState = CalibrationState.NOT_CALIBRATED;
		Debug.Log ("CalState: CalibrateLeft() Starting CMD");
		while (DigiBit.PrimaryData.CalState == CalibrationState.NOT_CALIBRATED ||
		       DigiBit.SecondaryData.CalState == CalibrationState.NOT_CALIBRATED) {
			DigiBit.CalibrateLeft ();
			yield return new WaitForSecondsRealtime (0.3f);
		}
		Debug.Log ("CalState: CalibrateLeft() CMD Rx'ed");

		while (PlayerStage == CalibrationStage.CalibratingLeft) {		
			if ((LeftPanel.transform.position - posEnd).sqrMagnitude > smallNumber) {
				LeftPanel.transform.position = Vector3.MoveTowards (LeftPanel.transform.position, posEnd, digitAnimationSpeed * Time.deltaTime);
			} else if ((LeftPanel.transform.position - leftPosStart).sqrMagnitude > smallNumber) {
				posEnd = leftPosStart;
			} else {
				posEnd = leftPosStart;
				posEnd.y += digitAnimationDistance;

				SetLEDs (Devices.PRIMARY, OFF, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				SetLEDs (Devices.PRIMARY, ONGREEN, OFF, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, OFF);
				yield return new WaitForSecondsRealtime (0.25f);
				SetLEDs (Devices.PRIMARY, ONGREEN, ONGREEN, ONGREEN);
				yield return new WaitForSecondsRealtime (0.25f);
			}

			yield return null;

			if (PlayerStage == CalibrationStage.CalibratingLeft && 
                   DigiBit.SecondaryData.CalState == CalibrationState.SUCCESS_FINISHED && DigiBit.PrimaryData.CalState == CalibrationState.SUCCESS_FINISHED) {
				PlayerStage = CalibrationStage.Done;						
				Debug.Log ("CalState: CalibrationState.SUCCESS_FINISHED");
			}
		}
	}

	IEnumerator HandsFollowAnimation()
	{
		var rightStart = RightPanel.transform.position;
		var rightEnd = rightStart;
		rightEnd.y =+ digitAnimationDistance;
		bool rightMoveForward = false;
		bool rightMoveBackward = false;

		var leftStart = LeftPanel.transform.position;
		var leftEnd = leftStart;
		leftEnd.y =+ digitAnimationDistance;
		bool leftMoveForward = false;
		bool leftMoveBackward = false;

		while (!EndScene) {
			// Secondary is the Right
			if (DigiBit.SecondaryData.MoveForward || rightMoveForward) {
				rightMoveForward = true;
				if ((RightPanel.transform.position - rightEnd).sqrMagnitude > smallNumber) 
					RightPanel.transform.position = Vector3.MoveTowards (RightPanel.transform.position, rightEnd, digitAnimationSpeed * Time.deltaTime);
				else
					rightMoveForward = false;				
			}
			else if (DigiBit.SecondaryData.MoveBackward || rightMoveBackward) {
				rightMoveBackward = true;
				if ((RightPanel.transform.position - rightStart).sqrMagnitude > smallNumber)
					RightPanel.transform.position = Vector3.MoveTowards (RightPanel.transform.position, rightStart, digitAnimationSpeed * Time.deltaTime);
				else
					rightMoveBackward = false;
			}

			if (DigiBit.PrimaryData.MoveForward || leftMoveForward) {
				leftMoveForward = true;
				if ((LeftPanel.transform.position - leftEnd).sqrMagnitude > smallNumber)
					LeftPanel.transform.position = Vector3.MoveTowards (LeftPanel.transform.position, leftEnd, digitAnimationSpeed * Time.deltaTime);
				else
					leftMoveForward = false;
			}
			else if (DigiBit.PrimaryData.MoveBackward || leftMoveBackward) {
				leftMoveBackward = true;
				if ((LeftPanel.transform.position - leftStart).sqrMagnitude > smallNumber)
					LeftPanel.transform.position = Vector3.MoveTowards (LeftPanel.transform.position, leftStart, digitAnimationSpeed * Time.deltaTime);
				else
					leftMoveBackward = false;
			}
								
			yield return null;
		}
	}
}
