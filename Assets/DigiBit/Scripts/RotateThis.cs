﻿using UnityEngine;
using System.Collections;

public class RotateThis : MonoBehaviour
{
    public Vector3 rotateVector = new Vector3(0, 0, -1);
    public float speed = 5.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update ()
    {
        transform.Rotate(rotateVector * speed);
	}
}
