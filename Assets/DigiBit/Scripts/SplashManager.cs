﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DigiBitSDK;
using UnityEngine.SceneManagement;
using UnityEngine.XR;

public class SplashManager : MonoBehaviour
{

     public Image LogoImg;

     void Start()
     {
          DigiBit.Init(false, false);
          StartCoroutine(startSplash());
     }


     IEnumerator startSplash()
     {
          while (!DigiBit.PrimaryConnected && !DigiBit.SecondaryConnected)
               yield return new WaitForSeconds(0.5f);

          LogoImg.canvasRenderer.SetAlpha(0.0f);

          FadeIn();
          yield return new WaitForSeconds(2.0f);

          FadeOut();
          yield return new WaitForSeconds(1.0f);
          StartCoroutine(LoadDevice("Cardboard"));


     }


     void FadeIn()
     {
          LogoImg.CrossFadeAlpha(1.0f, 1.5f, false);
     }


     void FadeOut()
     {
          LogoImg.CrossFadeAlpha(0.0f, 1.5f, false);
     }


     IEnumerator LoadDevice(string newDevice)
     {
          XRSettings.LoadDeviceByName(newDevice);
          yield return null;
          XRSettings.enabled = true;
          Debug.Log("UNITY LOGS VR DEVICE::" + XRSettings.loadedDeviceName);
          SceneManager.LoadScene("basketBallScene");
     }
}